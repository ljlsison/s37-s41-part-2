const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes.js')
const courseRoutes = require('./routes/courseRoutes.js')

const auth = require('./auth.js')

dotenv.config()

const app = express()
const port = 8001

// mongoDB connection
mongoose.connect(`mongodb+srv://cinnamontoast:${process.env.MONGODB_PASSWORD}@cluster0.4pgcafz.mongodb.net/booking-system-api?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.on('open',() => console.log(`Connected to MongoDB!`))
// mongoDB connection END

// To avoid CORS errors when trying to send our request to our server
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))

//Routes
app.use('/users', userRoutes)
app.use('/courses', courseRoutes)
//Routes END


app.listen(port, () => console.log(`Server is running at localHost:${port}`))