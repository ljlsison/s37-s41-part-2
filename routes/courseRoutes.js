const express = require('express')
const router = express.Router()
const CourseController = require(`../controllers/CourseController.js`)
const auth = require(`../auth.js`)

// Create Single Course
router.post('/create', auth.verify, (request, response) => {
    const data = {
        course: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    CourseController.addCourse(data).then((result) => {
        response.send(result)
    })
})

// Get all courses
router.get('/',(request,response) => {
    CourseController.getAllCourses().then((result) => {
        response.send(result)
    })
})

// Get active courses
router.get('/active-courses', (request, response) => {
    CourseController.getActiveCourses().then((result) => {
        response.send(result)
    })
})

// Get single course
router.get('/:courseID', (request,response) => {
    CourseController.getCourse(request.params.courseID).then((result) => {
        response.send(result)
    })
})

// Update course
router.patch('/:courseID/update', auth.verify, (request,response) => {
    CourseController.updateCourse(request.params.courseID, request.body).then((result) => {
        response.send(result)
    })
})

router.patch('/:courseID/archive', auth.verify, (request,response) => {
    CourseController.archiveCourse(request.params.courseID).then((result) => {
        response.send(result)
    })
})


// 
module.exports = router