const express = require('express')
const router = express.Router()
const UserController = require(`../controllers/UserController.js`)
const auth = require('../auth.js')

// check if email exists
router.post('/check-email', (request,response) => {
    UserController.checkIfEmailExists(request.body).then((result) => {
        response.send(result)
    })
})

// register new user
router.post('/register', (request,response) => {
    UserController.register(request.body).then((result) => {
        response.send(result)
    })
})

// Login User
router.post('/login', (request, response) => {
    UserController.login(request.body).then((result) =>{
        response.send(result)
    })
})


// Get Single user
router.get('/:id/details', auth.verify ,(request,response) => {
    UserController.getUserDetails(request.params.id).then((result) => {
        response.send(result)
    })
})

// Enroll a user
router.post('/enroll', auth.verify, (request, response) => {
    let data = {
        userID: request.body.userID,
        courseID: request.body.courseID
    }
    UserController.enroll(data).then((result) => {
        response.send(result)
    })
})


module.exports = router